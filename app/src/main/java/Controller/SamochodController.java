package Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import Model.Samochod;

import static Controller.SamochodController.FeedReaderContract.FeedEntry.COLUMN_NAME_MARKA;
import static Controller.SamochodController.FeedReaderContract.FeedEntry.COLUMN_NAME_MODEL;
import static Controller.SamochodController.FeedReaderContract.FeedEntry.COLUMN_NAME_ROCZNIK;
import static Controller.SamochodController.FeedReaderContract.FeedEntry.TABLE_NAME;

public class SamochodController extends SQLiteOpenHelper {

    final class FeedReaderContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private FeedReaderContract() {}

        /* Inner class that defines the table contents */
        class FeedEntry implements BaseColumns {
            static final String TABLE_NAME = "Samochod";
            static final String COLUMN_NAME_MARKA = "marka";
            static final String COLUMN_NAME_MODEL = "model";
            static final String COLUMN_NAME_ROCZNIK = "rocznik";
        }
    }

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Test.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String DATE_TYPE = " DATE";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    FeedReaderContract.FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    COLUMN_NAME_MARKA + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_MODEL + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_ROCZNIK + DATE_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public SamochodController(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public SQLiteDatabase open(){
        return this.getWritableDatabase();
    }

    public void insert(Samochod car){
        ContentValues dane = new ContentValues();
        dane.put(COLUMN_NAME_MARKA, car.getMarka());
        dane.put(COLUMN_NAME_MODEL, car.getModel());
        dane.put(COLUMN_NAME_ROCZNIK, car.getRocznik());
        this.getWritableDatabase().insertOrThrow(TABLE_NAME,null,dane);
    }

    public void delete(int id){
        this.getWritableDatabase().delete(TABLE_NAME, FeedReaderContract.FeedEntry._ID + " = " + id, null);
    }

    public void deleteAll(){
        this.getWritableDatabase().execSQL(SQL_DELETE_ENTRIES);
        onCreate(this.open());
    }

    public void update(Samochod car){
        ContentValues dane = new ContentValues();
        dane.put(COLUMN_NAME_MARKA, car.getMarka());
        dane.put(COLUMN_NAME_MODEL, car.getModel());
        dane.put(COLUMN_NAME_ROCZNIK, car.getRocznik());
        this.getWritableDatabase().update(TABLE_NAME,dane,FeedReaderContract.FeedEntry._ID + " = " + car.getId(), null);
    }

    private Cursor getSamochod(int id){
        String[] projection = {
                FeedReaderContract.FeedEntry._ID,
                COLUMN_NAME_MARKA,
                COLUMN_NAME_MODEL,
                COLUMN_NAME_ROCZNIK
        };
        return this.getReadableDatabase().query(TABLE_NAME, projection, FeedReaderContract.FeedEntry._ID + " = " + id, null, null, null, null);
    }

    public Samochod getSamochodObject(int id){
        Cursor lista = getSamochod(id);
        lista.getColumnCount();
        Samochod wynik = new Samochod();
        if ( lista != null && lista.moveToFirst()) {
            wynik.setId(lista.getInt(lista.getColumnIndex(FeedReaderContract.FeedEntry._ID)));
            wynik.setMarka(lista.getString(lista.getColumnIndex(COLUMN_NAME_MARKA)));
            wynik.setModel(lista.getString(lista.getColumnIndex(COLUMN_NAME_MODEL)));
            wynik.setRocznik(lista.getString(lista.getColumnIndex(COLUMN_NAME_ROCZNIK)));
            lista.close();
        }
        return wynik;
    }

    public Cursor listAll(){
        String[] projection = {
                FeedReaderContract.FeedEntry._ID,
                COLUMN_NAME_MARKA,
                COLUMN_NAME_MODEL,
                COLUMN_NAME_ROCZNIK
        };

        /*
        // Filter results WHERE "title" = 'My Title'
        String selection = COLUMN_NAME_MARKA + " = ?";
        String[] selectionArgs = { "My Title" };
        */

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                COLUMN_NAME_MARKA + " DESC";

        return this.getReadableDatabase().query(
                TABLE_NAME,                               // The table to query
                projection,                               // The columns to return
                null,
                null,
                //selection,                                // The columns for the WHERE clause
                //selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
    }
}
