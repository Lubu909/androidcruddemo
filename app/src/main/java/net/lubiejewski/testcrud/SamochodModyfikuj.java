package net.lubiejewski.testcrud;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import Controller.SamochodController;
import Model.Samochod;

public class SamochodModyfikuj extends AppCompatActivity {

    SamochodController DB;
    int id;
    EditText markaPole;
    EditText modelPole;
    EditText rocznikPole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samochod_dodaj);
        DB = new SamochodController(this);
        DB.open();

        id = getIntent().getIntExtra("idSamochod",0);
        markaPole = (EditText) findViewById(R.id.MarkaPole);
        modelPole = (EditText) findViewById(R.id.ModelPole);
        rocznikPole = (EditText) findViewById(R.id.RocznikPole);

        setValues();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.dodajFormularz);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modActivity();
            }
        });
    }

    private void setValues(){
        Samochod dane = DB.getSamochodObject(id);

        markaPole.setText(dane.getMarka(), TextView.BufferType.EDITABLE);
        modelPole.setText(dane.getModel(), TextView.BufferType.EDITABLE);
        rocznikPole.setText(dane.getRocznik(), TextView.BufferType.EDITABLE);
    }

    private void getValues(){
        Samochod dane = new Samochod();

        String marka = markaPole.getText().toString();
        String model = modelPole.getText().toString();
        String rocznik = rocznikPole.getText().toString();

        dane.setId(id);
        dane.setMarka(marka);
        dane.setModel(model);
        dane.setRocznik(rocznik);

        DB.update(dane);
    }

    private void modActivity(){
        getValues();
        DB.close();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
