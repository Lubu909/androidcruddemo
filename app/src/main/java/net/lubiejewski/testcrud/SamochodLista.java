package net.lubiejewski.testcrud;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import Controller.SamochodController;

public class SamochodLista extends AppCompatActivity {

    SamochodController DB;

    ListView list;
    int wybrany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samochod_lista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        openDB();
        populateListView();

        list = (ListView) findViewById(R.id.listaSamochod);
        /*list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onCreatePopupMenu(view,l);
                //Log.d("Int i: " + i, "cokolwiek");
                //Log.d("Long l: " + l, "cokolwiek2");
            }
        });*/
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l){
                onCreatePopupMenu(view,l);
                return true;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivityDodaj();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_samochod_lista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            getActivityDodaj();
        }

        if (id == R.id.action_purge){
            DB.deleteAll();
            populateListView();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                populateListView();
            }
        }
    }

    public boolean onCreatePopupMenu(View view, long position){
        PopupMenu popup = new PopupMenu(SamochodLista.this,view);

        wybrany = (int) position;

        popup.getMenuInflater().inflate(R.menu.menu_samochod_item, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                onPopupMenuItemClick(item);

                return SamochodLista.super.onMenuItemSelected(id, item);
            }
        });
        popup.show();
        return true;
    }

    private void onPopupMenuItemClick(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.itemModify) {
            getActivityModyfikuj();
        }

        if (id == R.id.itemDelete){
            DB.delete(wybrany);
            populateListView();
        }
    }

    private void getActivityModyfikuj(){
        Intent modyfikuj = new Intent(this, SamochodModyfikuj.class);
        modyfikuj.putExtra("idSamochod", wybrany);
        startActivityForResult(modyfikuj, 1);
    }

    private void getActivityDodaj(){
        Intent dodaj = new Intent(this, SamochodDodaj.class);
        startActivityForResult(dodaj, 1);
    }

    private void openDB(){
        DB = new SamochodController(this);
        DB.open();
    }

    private void populateListView(){
        Cursor lista = DB.listAll();
        String FromFieldNames[] = new String[] {"_id", "marka", "model", "rocznik"};
        int[] toIds = new int[] {R.id.ItemID, R.id.Marka, R.id.Model, R.id.Rocznik};
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getBaseContext(),R.layout.item_samochod_lista, lista, FromFieldNames, toIds, 0);
        list = (ListView) findViewById(R.id.listaSamochod);
        list.setAdapter(adapter);
    }
}
