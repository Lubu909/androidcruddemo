package net.lubiejewski.testcrud;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import Controller.SamochodController;
import Model.Samochod;

public class SamochodDodaj extends AppCompatActivity {

    SamochodController DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samochod_dodaj);
        DB = new SamochodController(this);
        DB.open();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.dodajFormularz);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity();
            }
        });
    }

    private void getValues(){
        Samochod dane = new Samochod();
        EditText markaPole = (EditText) findViewById(R.id.MarkaPole);
        EditText modelPole = (EditText) findViewById(R.id.ModelPole);
        EditText rocznikPole = (EditText) findViewById(R.id.RocznikPole);
        String marka = markaPole.getText().toString();
        String model = modelPole.getText().toString();
        String rocznik = rocznikPole.getText().toString();

        dane.setMarka(marka);
        dane.setModel(model);
        dane.setRocznik(rocznik);

        DB.insert(dane);
    }

    private void addActivity(){
        getValues();
        DB.close();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
