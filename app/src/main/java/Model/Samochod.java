package Model;

import java.sql.Date;

/**
 * Created by Lubu909 on 2016-12-08.
 */

public class Samochod {
    private int id;
    private String marka;
    private String model;
    private String rocznik;

    public Samochod() {
    }

    public Samochod(int id, String marka, String model, String rocznik) {
        this.id = id;
        this.marka = marka;
        this.model = model;
        this.rocznik = rocznik;
    }

    public Samochod(String model, String marka, String rocznik) {
        this.model = model;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRocznik() {
        return rocznik;
    }

    public void setRocznik(String rocznik) {
        this.rocznik = rocznik;
    }
}
